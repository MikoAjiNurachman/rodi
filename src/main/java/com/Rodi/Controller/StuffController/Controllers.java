package com.Rodi.Controller.StuffController;

import com.Rodi.Model.Koneksi;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

@Controller
public class Controllers {
    ModelAndView mv = new ModelAndView();
    Koneksi conn = new Koneksi();
    Connection c = conn.config();
    @RequestMapping("/mahasiswa")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        try {
            Statement statement = c.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT mahasiswa.*,mata_pelajaran.* FROM mata_pelajaran JOIN mahasiswa USING(id_pelajaran)");
            this.mv.setViewName("Mahasiswa/index.jsp");
            this.mv.addObject("data", resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/createMahasiswa")
    public ModelAndView createMahasiswa(HttpServletRequest request, HttpServletResponse response) {
        try {
            Statement statement = c.createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM mata_pelajaran");
            this.mv.setViewName("Mahasiswa/create.jsp");
            this.mv.addObject("data", res);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/storeMahasiswa")
    public ModelAndView storeMahasiswa(HttpServletRequest req, HttpServletResponse res) {
        try {
            Statement statement = c.createStatement();
            int id_mapel = Integer.parseInt(req.getParameter("id_mapel"));
            String nama = req.getParameter("nama");
            String jurusan = req.getParameter("jurusan");
            int semester = Integer.parseInt(req.getParameter("semester"));
            String fakultas = req.getParameter("fakultas");
            int nilai = Integer.parseInt(req.getParameter("nilai"));
            statement.execute("INSERT INTO mahasiswa(id_pelajaran,nama_mahasiswa,jurusan,semester,fakultas,nilai) VALUES('"+id_mapel+"','"+nama+"','"+jurusan+"','"+semester+"','"+fakultas+"','"+nilai+"')");
            mv.setViewName("Mahasiswa/success.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("editMahasiswa")
    public ModelAndView editMahasiswa(HttpServletRequest req, HttpServletResponse res) {
        int id_mahasiswa = Integer.parseInt(req.getParameter("id"));
        try {
            Statement state = c.createStatement();
            ResultSet resultSet = state.executeQuery("SELECT * FROM mahasiswa WHERE id_mahasiswa='"+id_mahasiswa+"'");
            mv.setViewName("Mahasiswa/edit.jsp");
            mv.addObject("data", resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/updateMahasiswa")
    public ModelAndView updateMahasiswa(HttpServletRequest req, HttpServletResponse res) {
        try {
            Statement state = c.createStatement();
            int id_mahasiswa = Integer.parseInt(req.getParameter("id_mahasiswa"));
            int id_mapel = Integer.parseInt(req.getParameter("id_mapel"));
            String nama = req.getParameter("nama");
            String jurusan = req.getParameter("jurusan");
            int semester = Integer.parseInt(req.getParameter("semester"));
            String fakultas = req.getParameter("fakultas");
            int nilai = Integer.parseInt(req.getParameter("nilai"));
            state.execute("UPDATE mahasiswa SET id_pelajaran='"+id_mapel+"',nama_mahasiswa='"+nama+"',jurusan='"+jurusan+"',semester='"+semester+"',fakultas='"+fakultas+"',nilai='"+nilai+"' WHERE id_mahasiswa='"+id_mahasiswa+"'");
            mv.setViewName("Mahasiswa/success.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/deleteMahasiswa")
    public ModelAndView deleteMahasiswa(HttpServletRequest req, HttpServletResponse res) {
        try {
            Statement state = c.createStatement();
            int id_mahasiswa = Integer.parseInt(req.getParameter("id"));
            state.execute("DELETE FROM mahasiswa WHERE id_mahasiswa='"+id_mahasiswa+"'");
            mv.setViewName("Mahasiswa/success.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/pengajar")
    public ModelAndView pengajar(HttpServletRequest req, HttpServletResponse res) {
        try {
            Statement state = c.createStatement();
            ResultSet resultSet = state.executeQuery("SELECT pengajar.*,mata_pelajaran.* FROM mata_pelajaran JOIN pengajar USING(id_pelajaran)");
            mv.setViewName("Pengajar/index.jsp");
            mv.addObject("data", resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/createPengajar")
    public ModelAndView createPengajar(HttpServletRequest request, HttpServletResponse response) {
        try {
            Statement statement = c.createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM mata_pelajaran");
            this.mv.setViewName("Pengajar/create.jsp");
            this.mv.addObject("data", res);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/storePengajar")
    public ModelAndView storePengajar(HttpServletRequest req, HttpServletResponse res) {
        try {
            Statement statement = c.createStatement();
            int id_mapel = Integer.parseInt(req.getParameter("id_mapel"));
            String nama = req.getParameter("nama");
            int tahun = Integer.parseInt(req.getParameter("tahun"));
            String jadwal = req.getParameter("jadwal");
            statement.execute("INSERT INTO pengajar(nama_pengajar,id_pelajaran,tahun,jadwal) VALUES('"+nama+"','"+id_mapel+"','"+tahun+"','"+jadwal+"')");
            mv.setViewName("Pengajar/success.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("editPengajar")
    public ModelAndView editPengajar(HttpServletRequest req, HttpServletResponse res) {
        int id_pengajar = Integer.parseInt(req.getParameter("id"));
        try {
            Statement state = c.createStatement();
            ResultSet resultSet = state.executeQuery("SELECT * FROM pengajar WHERE id_pengajar='"+id_pengajar+"'");
            mv.setViewName("Pengajar/edit.jsp");
            mv.addObject("data", resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/updatePengajar")
    public ModelAndView updatePengajar(HttpServletRequest req, HttpServletResponse res) {
        try {
            Statement state = c.createStatement();
            int id_pengajar = Integer.parseInt(req.getParameter("id_pengajar"));
            int id_mapel = Integer.parseInt(req.getParameter("id_mapel"));
            String nama = req.getParameter("nama");
            int tahun = Integer.parseInt(req.getParameter("tahun"));
            String jadwal = req.getParameter("jadwal");
            state.execute("UPDATE pengajar SET id_pelajaran='"+id_mapel+"',nama_pengajar='"+nama+"',tahun='"+tahun+"',jadwal='"+jadwal+"' WHERE id_pengajar='"+id_pengajar+"'");
            mv.setViewName("Pengajar/success.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/deletePengajar")
    public ModelAndView deletePengajar(HttpServletRequest req, HttpServletResponse res) {
        try {
            Statement state = c.createStatement();
            int id_pengajar = Integer.parseInt(req.getParameter("id"));
            state.execute("DELETE FROM pengajar WHERE id_pengajar='"+id_pengajar+"'");
            mv.setViewName("Pengajar/success.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/gradeMahasiswa")
    public ModelAndView gradeMahasiswa(HttpServletRequest req, HttpServletResponse res) {
        try {
            Statement statement = c.createStatement();
            int id_pelajaran = Integer.parseInt(req.getParameter("id"));
            ResultSet resultSet = statement.executeQuery("SELECT * FROM mahasiswa WHERE id_pelajaran='"+id_pelajaran+"'");
            mv.setViewName("Pengajar/grade.jsp");
            mv.addObject("data", resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/gradePost")
    public ModelAndView gradePost(HttpServletRequest req, HttpServletResponse res) {
        int id_mahasiswa = Integer.parseInt(req.getParameter("id_mahasiswa"));
        int nilai = Integer.parseInt(req.getParameter("nilai"));
        try {
            Statement state = c.createStatement();
            state.execute("UPDATE mahasiswa SET nilai='"+nilai+"' WHERE id_mahasiswa='"+id_mahasiswa+"'");
            mv.setViewName("Mahasiswa/success.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
    @RequestMapping("/showAll")
    public ModelAndView showAll() {
        try {
            Statement statement = c.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT mahasiswa.*,pengajar.*,mata_pelajaran.* FROM mata_pelajaran JOIN mahasiswa USING(id_pelajaran) JOIN pengajar USING(id_pelajaran) ORDER BY nama_pelajaran");
            mv.setViewName("showAll.jsp");
            mv.addObject("data",resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }
}
