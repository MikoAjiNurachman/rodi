package com.Rodi.Model;

import java.sql.Connection;
import java.sql.DriverManager;

public class Koneksi {
    private static Connection connection;
    public static Connection config() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/unch","root","");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void main(String[] args) {
        config();
    }
}
