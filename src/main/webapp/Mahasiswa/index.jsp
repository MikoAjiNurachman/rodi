<html>
    <head>
        <title>Mahasiswa</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
        <h1>Data Mahasiswa</h1>
        <a class="btn btn-primary" href="/rodi/createMahasiswa">Buat Data</a>
         <table class="mt-4 table table-hover table-dark">
             <thead>
                 <tr>
                     <th>Mata Pelajaran</th>
                     <th>Nama Mahasiswa</th>
                     <th>Jurusan</th>
                     <th>Semester</th>
                     <th>Fakultas</th>
                     <th>Nilai</th>
                     <th>Opsi</th>
                 </tr>
             </thead>
             <tbody>
             <%@ page import="java.sql.ResultSet"%>
             <% ResultSet result = (ResultSet) request.getAttribute("data"); %>
             <% while(result.next()) {%>
                 <tr>
                     <td><%= result.getString("nama_pelajaran")%></td>
                     <td><%= result.getString("nama_mahasiswa")%></td>
                     <td><%= result.getString("jurusan")%></td>
                     <td><%= result.getInt("semester")%></td>
                     <td><%= result.getString("fakultas")%></td>
                     <td><%= result.getInt("nilai")%></td>
                     <td>
                        <a class="btn btn-warning" href="/rodi/editMahasiswa?id=<%= result.getInt("id_mahasiswa")%>">Edit</a>
                        <a class="btn btn-danger" onclick="return confirm('Yakin Ingin Dihapus ?')" href="/rodi/deleteMahasiswa?id=<%= result.getInt("id_mahasiswa")%>">Delete</a>
                     </td>
                 </tr>
             <% } %>
             </tbody>
         </table>
         </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>