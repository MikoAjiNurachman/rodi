<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.Statement"%>
<%@ page import="com.Rodi.Model.Koneksi"%>
<%
        Koneksi conn = new Koneksi();
        Connection c = conn.config();
%>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Pengajar</title>
</head>
<body>
    <% ResultSet resultSet = (ResultSet) request.getAttribute("data");
       resultSet.next();
    %>
<div class="container mt-5">
<a class="btn btn-secondary mb-3" href="/rodi/pengajar">Back to Index</a>
    <form action="/rodi/updatePengajar" method="POST">
        <div class="form-group">
            <label for="">Nama Pengajar</label>
            <input class="form-control" type="text" value="<%= resultSet.getString("nama_pengajar")%>" name="nama">
        </div>
        <div class="form-group">
             <input type="hidden" value="<%= resultSet.getInt("id_pengajar")%>" name="id_pengajar"/>
             <label for="">Mata Pelajaran</label>
                <select class="form-control" name="id_mapel">
                    <option disabled selected>Choose</option>
                    <% Statement statement = c.createStatement(); %>
                    <% ResultSet result = statement.executeQuery("SELECT * FROM mata_pelajaran"); %>
                    <% while(result.next()) { %>
                    <option <% if (resultSet.getInt("id_pelajaran") == result.getInt("id_pelajaran")) { %> selected <% } %> value="<%= result.getString("id_pelajaran")%>"><%= result.getString("nama_pelajaran")%></option>
                    <% } %>
                </select>
        </div>
        <div class="form-group">
            <label for="">Tahun Mulai Mengajar</label>
            <input class="form-control" type="number" value="<%= resultSet.getInt("tahun")%>" name="tahun">
        </div>
        <div class="form-group">
            <label for="">Jadwal</label>
            <input class="form-control" type="text" value="<%= resultSet.getString("jadwal")%>" name="jadwal">
        </div>
        <button class="btn btn-warning btn-block" type="submit">Edit</button>
    </form>
</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>