<%@ page import="java.sql.ResultSet"%>
<html>
<head>
    <title>Grade Mahasiswa</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
    <a class="btn btn-secondary mb-3" href="/rodi/pengajar">Back to Index</a>
    <form action="/rodi/gradePost" method="POST">
        <div class="form-group">
        <label for="">Nama Mahasiswa</label>
        <select class="form-control" name="id_mahasiswa">
            <option disabled selected>Choose</option>
            <% ResultSet result = (ResultSet) request.getAttribute("data"); %>
            <% while(result.next()) { %>
            <option value="<%= result.getInt("id_mahasiswa")%>"><%= result.getString("nama_mahasiswa")%></option>
            <% } %>
        </select>
        </div>
        <div class="form-group">
            <label for="">Masukkan Nilai</label>
            <input class="form-control" type="number" name="nilai">
        </div>
        <button class="btn btn-success btn-block" type="submit">Grade !</button>
    </form>
    </div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>