<html>
    <head>
        <title>Pengajar</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
        <h1>Data Pengajar</h1>
        <a class="btn btn-primary" href="/rodi/createPengajar">Buat Data</a>
         <table class="mt-4 table table-hover table-secondary">
             <thead>
                 <tr>
                     <th>Nama Pengajar</th>
                     <th>Mata Pelajaran</th>
                     <th>Tahun Mulai Mengajar</th>
                     <th>Jadwal Mengajar</th>
                     <th>Opsi</th>
                 </tr>
             </thead>
             <tbody>
             <%@ page import="java.sql.ResultSet"%>
             <% ResultSet result = (ResultSet) request.getAttribute("data"); %>
             <% while(result.next()) {%>
                 <tr>
                     <td><%= result.getString("nama_pengajar")%></td>
                     <td><%= result.getString("nama_pelajaran")%></td>
                     <td><%= result.getInt("tahun")%></td>
                     <td><%= result.getString("jadwal")%></td>
                     <td>
                        <a class="btn btn-warning" href="/rodi/editPengajar?id=<%= result.getInt("id_pengajar")%>">Edit</a>
                        <a class="btn btn-success" href="/rodi/gradeMahasiswa?id=<%= result.getInt("id_pelajaran")%>">Nilai Mahasiswa</a>
                        <a class="btn btn-danger" onclick="return confirm('Yakin Ingin Dihapus ?')" href="/rodi/deletePengajar?id=<%= result.getInt("id_pengajar")%>">Delete</a>
                     </td>
                 </tr>
             <% } %>
             </tbody>
         </table>
         </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>